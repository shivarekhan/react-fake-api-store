import React, { Component } from 'react'
import axios from 'axios';
import Loader from '../Loader/Loader';
import "./ProductPage.css"


class ProductPage extends Component {
    constructor(props) {
        super(props);
        let { id } = props.match.params;
        this.state = {
            fetchedProducts: {},
            apiFetchError: false,
            errorMsg: "",
            isLoading: false,
            isFetchFailed: false,
            failedMsg: "",
        };
        this.API_ENDPOINT = `https://fakestoreapi.com/products/${id}`;
    }
    fetchedProductsViaApi = () => {

        axios.get(this.API_ENDPOINT)
            .then((response) => {
                if (!response.data) {
                    this.setState({
                        apiFetchError: true,
                        failedMsg: "!! NO product available for this id.",
                        fetchedProducts: {},
                        isLoading: false
                    })

                } else {
                    const fetchedProducts = response.data;
                    this.setState({
                        fetchedProducts,
                        isLoading: false,
                        apiFetchError: false,
                    });
                }
            })
            .catch((error) => {
                this.setState({
                    apiFetchError: true,
                    failedMsg: "Api fetch failed Please try after some time.",
                    fetchedProducts: {},
                    isLoading: false
                })
            })
    }
    render() {
        return (
            <>
                {this.state.isLoading ?
                    <Loader /> : this.state.apiFetchError ?
                        <h1>{this.state.failedMsg}</h1> : <div className='single-product'>
                            <div className='image-container'>
                                <img src={this.state.fetchedProducts.image} />
                            </div>
                            <div className='details'>

                                <div className='category'>{this.state.fetchedProducts.category}</div>
                                <div className="card-title">{this.state.fetchedProducts.title}</div>
                                <div className='description'>{this.state.fetchedProducts.description}</div>
                                <div className="price">Price:- ${this.state.fetchedProducts.price}</div>
                                <div className="button-container">
                                    <button>Buy Now</button>
                                    <button>Add To Cart</button>
                                </div>
                            </div>
                        </div>}

            </>
        )
    }
    componentDidMount() {
        this.setState({
            isLoading: true
        }, this.fetchedProductsViaApi);
    }
}

export default ProductPage;
