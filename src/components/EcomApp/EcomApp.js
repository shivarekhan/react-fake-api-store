import React, { Component } from 'react'
import axios from 'axios';
import { BrowserRouter, Route, Router, Switch } from 'react-router-dom';
import Navbar from '../Navbar/Navbar';
import Card from '../Card/Card';
import Loader from '../Loader/Loader';
import ShowProduct from '../ShowProduct/ShowProduct';
import "./EcomApp.css"
import ProductPage from '../ProductPage/ProductPage';


class EcomApp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fetchedProducts: [],
            apiFetchError: false,
            errorMsg: "",
            isLoading: false,
            isFetchFailed: false,
            failedMsg: "",
        };
        this.API_ENDPOINT = "https://fakestoreapi.com/products";
    }

    fetchedProductsViaApi = () => {

        axios.get(this.API_ENDPOINT)
            .then((response) => {
                if (response.data.length === 0) {
                    this.setState({
                        apiFetchError: true,
                        failedMsg: "!! NO product to Show.",
                        fetchedProducts: [],
                        isLoading: false
                    })

                } else {
                    const fetchedProducts = response.data;
                    this.setState({
                        fetchedProducts,
                        isLoading: false,
                        apiFetchError: false,
                    });
                }
            })
            .catch((error) => {
                this.setState({
                    apiFetchError: true,
                    failedMsg: "Api fetch failed Please try after some time.",
                    fetchedProducts: [],
                    isLoading: false
                })
            })

    }
    render() {
        return (
            <>
                <div className='parent'>
                    <Navbar />
                    <BrowserRouter>
                        <Switch>
                            <Route exact path="/">
                                <ShowProduct
                                    fetchedProducts={this.state.fetchedProducts}
                                    isLoading={this.state.isLoading}
                                    failedMsg={this.state.failedMsg}
                                    apiFetchError={this.state.apiFetchError}
                                />
                            </Route>
                            <Route path="/product/:id" component={ProductPage} />
                        </Switch>
                    </BrowserRouter>
                </div>
            </>
        )
    }

    componentDidMount() {
        this.setState({
            isLoading: true
        }, this.fetchedProductsViaApi);
    }
}

export default EcomApp;